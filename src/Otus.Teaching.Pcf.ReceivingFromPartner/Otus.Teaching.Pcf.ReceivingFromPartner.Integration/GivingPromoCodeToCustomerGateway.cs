﻿using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Abstractions.Gateways;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Domain;
using Otus.Teaching.Pcf.ReceivingFromPartner.Integration.Dto;
using Otus.Teaching.Pcf.ReceivingFromPartner.Integration.Services;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.Integration
{
    public class GivingPromoCodeToCustomerGateway
        : IGivingPromoCodeToCustomerGateway
    {
        private readonly IRabbitMQService _service;
        private readonly IRabbitMQOptions _options;

        public GivingPromoCodeToCustomerGateway(IRabbitMQService service, IOptions<RabbitMQOptions> options)
        {
            _service = service;
            _options = options.Value;
        }
        
        public async Task GivePromoCodeToCustomer(PromoCode promoCode)
        {
            var dto = new GivePromoCodeToCustomerDto()
            {
                PartnerId = promoCode.Partner.Id,
                BeginDate = promoCode.BeginDate.ToShortDateString(),
                EndDate = promoCode.EndDate.ToShortDateString(),
                PreferenceId = promoCode.PreferenceId,
                PromoCode = promoCode.Code,
                ServiceInfo = promoCode.ServiceInfo,
                PartnerManagerId = promoCode.PartnerManagerId
            };

            var stringDto = Newtonsoft.Json.JsonConvert.SerializeObject(dto);
            
            _service.Send(_options.PromocodeQueueName,stringDto);

            await Task.CompletedTask;
        }
    }
}