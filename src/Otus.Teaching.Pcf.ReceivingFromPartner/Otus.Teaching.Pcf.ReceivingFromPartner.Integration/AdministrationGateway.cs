﻿using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Abstractions.Gateways;
using Otus.Teaching.Pcf.ReceivingFromPartner.Integration.Services;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.Integration
{
    public class AdministrationGateway
        : IAdministrationGateway
    {
        private readonly IRabbitMQService _service;
        private readonly IRabbitMQOptions _options;

        public AdministrationGateway(IRabbitMQService service, IOptions<RabbitMQOptions> options)
        {
            _service = service;
            _options = options.Value;
        }
        
        public async Task NotifyAdminAboutPartnerManagerPromoCode(Guid partnerManagerId)
        {
            _service.Send(_options.EmployeeQueueName,partnerManagerId.ToString());

            await Task.CompletedTask;
        }
    }
}