using System.Text;
using Microsoft.Extensions.Options;
using RabbitMQ.Client;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.Integration.Services
{
    public class RabbitMQService : IRabbitMQService
    {
        private readonly IRabbitMQOptions _options;

        public RabbitMQService(IOptions<RabbitMQOptions> options)
        {
            _options = options.Value;
        }

        public void Send(string queue, string message)
        {
            var factory = new ConnectionFactory()
            {
                HostName = _options.HostName,
                UserName = _options.UserName,
                Password = _options.Password
            };
            
            using var connection = factory.CreateConnection();
            using var channel = connection.CreateModel();

            channel.QueueDeclare(queue: queue,
                durable: false,
                exclusive: false,
                autoDelete: false,
                arguments: null);

            var body = Encoding.UTF8.GetBytes(message);

            channel.BasicPublish(exchange: "",
                routingKey: queue,
                basicProperties: null,
                body: body);
        }
    }
}