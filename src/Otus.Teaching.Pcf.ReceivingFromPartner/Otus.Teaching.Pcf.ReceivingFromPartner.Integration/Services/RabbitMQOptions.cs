namespace Otus.Teaching.Pcf.ReceivingFromPartner.Integration.Services
{
    public class RabbitMQOptions : IRabbitMQOptions
    {
        public string HostName { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string EmployeeQueueName { get; set; }
        public string PromocodeQueueName { get; set; }
    }
}