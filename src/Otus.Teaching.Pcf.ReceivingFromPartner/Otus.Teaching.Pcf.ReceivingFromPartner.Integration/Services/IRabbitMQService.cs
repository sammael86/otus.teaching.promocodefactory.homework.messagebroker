namespace Otus.Teaching.Pcf.ReceivingFromPartner.Integration.Services
{
    public interface IRabbitMQService
    {
        public void Send(string queue, string message);
    }
}