using System;
using System.Threading.Tasks;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;

namespace Otus.Teaching.Pcf.Administration.Core.Abstractions.Services
{
    public interface IEmployeeService
    {
        public Task<Employee> UpdateAppliedPromocodesAsync(Guid id);
    }
}