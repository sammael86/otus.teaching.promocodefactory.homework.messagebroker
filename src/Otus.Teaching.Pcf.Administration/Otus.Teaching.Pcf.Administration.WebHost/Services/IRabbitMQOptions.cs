namespace Otus.Teaching.Pcf.Administration.WebHost.Services
{
    public interface IRabbitMQOptions
    {
        public string HostName { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string EmployeeQueueName { get; set; }
    }
}