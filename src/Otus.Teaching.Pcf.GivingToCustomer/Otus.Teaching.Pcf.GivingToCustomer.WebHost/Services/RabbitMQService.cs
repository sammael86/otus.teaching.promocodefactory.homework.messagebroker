using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Services;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Dto;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Services
{
    public class RabbitMQService : IHostedService
    {
        private readonly IPromocodeService _promocodeService;
        private readonly IRabbitMQOptions _options;

        private EventingBasicConsumer _consumer;
        private IConnection _connection;
        private IModel _channel;

        private readonly IServiceScope _scope;

        public RabbitMQService(IOptions<RabbitMQOptions> options, IServiceScopeFactory scopeFactory)
        {
            _scope = scopeFactory.CreateScope();
            _promocodeService = _scope.ServiceProvider.GetService<IPromocodeService>();
            _options = options.Value;
        }

        public async Task StartAsync(CancellationToken cancellationToken)
        {
            var factory = new ConnectionFactory()
            {
                HostName = _options.HostName,
                UserName = _options.UserName,
                Password = _options.Password
            };

            while (_connection is null)
            {
                try
                {
                    _connection = factory.CreateConnection();
                }
                catch
                {
                    await Task.Delay(1000, cancellationToken);
                }
            }

            _channel = _connection.CreateModel();

            _channel.QueueDeclare(_options.PromocodeQueueName,
                false,
                false,
                false,
                null);

            _consumer = new EventingBasicConsumer(_channel);
            _consumer.Received += HandleMessage;

            _channel.BasicConsume(_options.PromocodeQueueName,
                false,
                "promocodeTag",
                _consumer);

            await Task.CompletedTask;
        }

        public async Task StopAsync(CancellationToken cancellationToken)
        {
            _channel.BasicCancel("promocodeTag");
            _consumer.Received -= HandleMessage;

            _channel.Close();
            _connection.Close();

            _scope.Dispose();

            await Task.CompletedTask;
        }

        private async void HandleMessage(object model, BasicDeliverEventArgs ea)
        {
            var body = ea.Body.ToArray();
            var message = Encoding.UTF8.GetString(body);
            var request = Newtonsoft.Json.JsonConvert.DeserializeObject<GivePromoCodeRequest>(message);

            var result = await _promocodeService.GivePromoCodesToCustomersWithPreferenceAsync(request);

            if (result is null)
                _channel.BasicNack(ea.DeliveryTag, false, true);
            else
                _channel.BasicAck(ea.DeliveryTag, false);
        }
    }
}