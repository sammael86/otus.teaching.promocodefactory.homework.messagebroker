using System;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Services;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Dto;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Mappers;

namespace Otus.Teaching.Pcf.GivingToCustomer.Core.Services
{
    public class PromocodeService : IPromocodeService
    {
        private readonly IRepository<Preference> _preferencesRepository;
        private readonly IRepository<Customer> _customersRepository;
        private readonly IRepository<PromoCode> _promoCodesRepository;

        public PromocodeService(IRepository<Preference> preferencesRepository,
            IRepository<Customer> customersRepository,
            IRepository<PromoCode> promoCodesRepository)
        {
            _preferencesRepository = preferencesRepository;
            _customersRepository = customersRepository;
            _promoCodesRepository = promoCodesRepository;
        }

        public async Task<PromoCode> GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeRequest request)
        {
            //Получаем предпочтение по имени
            var preference = await _preferencesRepository.GetByIdAsync(request.PreferenceId);

            if (preference == null)
                return null;

            //  Получаем клиентов с этим предпочтением:
            var customers = await _customersRepository
                .GetWhere(d => d.Preferences.Any(x =>
                    x.Preference.Id == preference.Id));

            var promoCode = PromoCodeMapper.MapFromModel(request, preference, customers);

            await _promoCodesRepository.AddAsync(promoCode);

            return promoCode;
        }
    }
}