using System.Threading.Tasks;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Dto;

namespace Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Services
{
    public interface IPromocodeService
    {
        public Task<PromoCode> GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeRequest request);
    }
}